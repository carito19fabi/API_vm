﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Api_machineVirt.Models.Request
{
    public class ValidatorsController : ApiController
    {
        BDvirtual_mEntities entities = new BDvirtual_mEntities();
        public IEnumerable<Obtain_validations_Result> Get()
        {
            var validators = entities.Obtain_validations().ToList();
            return validators;
        }
    }
}
