﻿using Api_machineVirt.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Api_machineVirt.Controllers
{
    public class MachineVirtualController : ApiController
    {
        BDvirtual_mEntities entities = new BDvirtual_mEntities();
        public IEnumerable<Machine_virtual> Get()
        {
            return entities.Machine_virtual.ToList();
        }

        [HttpPost]
        public IHttpActionResult Add(Models.Request.Machine_virtRequest mv)
        {
            Machine_virtual m1 = new Machine_virtual();
            m1.ram = mv.ram;
            m1.processor = mv.processor;
            m1.SO = mv.SO;
            m1.vlan = mv.vlan;
            m1.id_per = mv.id_per;

            credentialss c1 = new credentialss();
            c1.password_adm = mv.password_adm;
            c1.password_us = mv.password_us;
            c1.id_machine = m1.id_machine;

            disks d1 = new disks();
            d1.tam_disk = mv.tam_disk;
            d1.id_typeDisk = mv.id_typeDisk;
            d1.id_machine = m1.id_machine;

            rent_date r1 = new rent_date();
            r1.star_date = mv.star_date;
            r1.end_date = mv.end_date;
            r1.request_date = mv.request_date;
            r1.id_machine = m1.id_machine;

            foreach (var validator in mv.validators)
            {
                Validations_ Aux_val = new Validations_();
                Aux_val.id_vm = m1.id_machine;
                Aux_val.id_personVal = validator;
                entities.Validations_.Add(Aux_val);
            }

            entities.Machine_virtual.Add(m1);
            entities.credentialss.Add(c1);
            entities.disks.Add(d1);
            entities.rent_date.Add(r1);
            entities.SaveChanges();

            return Ok("exito");

        }

        public Models.Request.Machine_virtRequest Get(string firstname, string name)
        {
            persons p1 = entities.persons.Where(p => p.firstname == firstname && p.name == name).First();

            Users u1 = entities.Users.Where(u => u.id_user == p1.id_person).First();

            type_user t1 = entities.type_user.Where(t => t.id_typeU == u1.id_user).First();

            Machine_virtual mv = entities.Machine_virtual.Where(m1 =>m1.id_per == p1.id_person).First();

            credentialss cr = entities.credentialss.Where(c => c.id_machine == mv.id_machine).First();

            disks di = entities.disks.Where(d => d.id_machine == mv.id_machine).First();

            type_d td = entities.type_d.Where(ty => ty.id_typed == di.id_typeDisk).First();

            rent_date rd = entities.rent_date.Where(r=> r.id_machine == mv.id_machine).First();
            Models.Request.Machine_virtRequest mvr = new Models.Request.Machine_virtRequest();
            mvr.ram = mv.ram;
            mvr.processor = mv.processor;
            mvr.SO = mv.SO;
            mvr.vlan = mv.vlan;
            mvr.password_adm = cr.password_adm;
            mvr.password_us = cr.password_us;
            mvr.tam_disk = di.tam_disk;
            mvr.type_d1 = td.type_d1;
            mvr.star_date = rd.star_date;
            mvr.end_date = rd.end_date;
            mvr.request_date = rd.request_date;       
            return mvr;


        }




    }
}
