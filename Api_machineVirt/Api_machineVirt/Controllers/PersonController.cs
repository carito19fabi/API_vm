﻿using Api_machineVirt.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;



namespace Api_machineVirt.Controllers
{
    public class PersonController : ApiController
    {
        BDvirtual_mEntities entities = new BDvirtual_mEntities();
        public IEnumerable<persons> Get()
        {
            return entities.persons.ToList();
        }

           

        [HttpPost]
        public IHttpActionResult Add(Models.Request.PersonRequest per_req)
        {
            persons p1 = new persons();
            p1.name = per_req.name;
            p1.firstname = per_req.firstname;
            p1.lastname = per_req.lastname;
            p1.phone = per_req.phone;
            p1.email = per_req.email;

            
            Users u1 = new Users();
            u1.id_user = per_req.id_person;
            u1.name_u = per_req.name_u;
            u1.passwords = per_req.passwords;
            u1.id_typeu = per_req.id_typeu;

            entities.persons.Add(p1);
            entities.Users.Add(u1);
            entities.SaveChanges();
            return Ok("Exito");

        }
        public Models.Request.PersonRequest Get(string firstname , string name)
        {
            persons p1 = entities.persons.Where(p=> p.firstname == firstname && p.name == name ).First();
                 
            Users u1 = entities.Users.Where(u => u.id_user == p1.id_person).First();
            type_user  t1 = entities.type_user.Where(t => t.id_typeU == u1.id_user).First();
            Models.Request.PersonRequest pu = new Models.Request.PersonRequest();
            pu.name = p1.name;
            pu.firstname = p1.firstname;
            pu.lastname = p1.lastname;
            pu.phone = p1.phone;
            pu.email = p1.email;
            pu.name_u = u1.name_u;
            pu.passwords = u1.passwords;
            pu.id_typeu = u1.id_typeu;
            pu.Type_us = t1.Type_us;
            return pu;

        }



        [ResponseType(typeof(void))]
        public IHttpActionResult Put(int id, Models.Request.PersonRequest per)
        {
            persons p1 = new persons();
            p1.id_person = id;
            p1.name = per.name;
            p1.firstname = per.firstname;
            p1.lastname = per.lastname;
            p1.phone = per.phone;
            p1.email = per.email;
            Users u1u = new Users();
            u1u.id_user = per.id_person;
            u1u.name_u = per.name_u;
            u1u.passwords = per.passwords;
            entities.Entry(p1).State = EntityState.Modified;
            entities.Entry(u1u).State = EntityState.Modified;
            entities.SaveChanges();

            return StatusCode(HttpStatusCode.NoContent);

          
        }



    }
}
