﻿using System;
using Api_machineVirt.Models;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Api_machineVirt.Controllers
{
    public class MVaplicantController : ApiController
    {
        BDvirtual_mEntities entities = new BDvirtual_mEntities();

        public Models.Request.Machine_ApliRequest Get(int id)
        {
            Machine_virtual mv = entities.Machine_virtual.Where(m1 => m1.id_machine == id).First();

            persons p1 = entities.persons.Where(p => p.id_person == mv.id_per).First();

            Users u1 = entities.Users.Where(u => u.id_user == p1.id_person).First();

            type_user t1 = entities.type_user.Where(t => t.id_typeU == u1.id_user).First();

            credentialss cr = entities.credentialss.Where(c => c.id_machine == mv.id_machine).First();

            disks di = entities.disks.Where(d => d.id_machine == mv.id_machine).First();

            type_d td = entities.type_d.Where(ty => ty.id_typed == di.id_typeDisk).First();
            Validations_ val = entities.Validations_.Where(va=> va.id_vm == mv.id_machine).First();

            rent_date rd = entities.rent_date.Where(r => r.id_machine == mv.id_machine).First();
            Models.Request.Machine_ApliRequest mvA = new Models.Request.Machine_ApliRequest();
            mvA.id_machine = mv.id_machine;
            mvA.ram = mv.ram;
            mvA.processor = mv.processor;
            mvA.SO = mv.SO;
            mvA.vlan = mv.vlan;
            mvA.password_adm = cr.password_adm;
            mvA.password_us = cr.password_us;
            mvA.tam_disk = di.tam_disk;
            mvA.type_d1 = td.type_d1;
            mvA.star_date = rd.star_date;
            mvA.end_date = rd.end_date;
            mvA.request_date = rd.request_date;

    

            var res  = entities.State_machine(val.state_val.ToString());
            int aux = 0;
            foreach (int item in res)
            {
                aux = item;
            }

            mvA.state_val = aux;

            mvA.name = p1.name;
            mvA.firstname = p1.name;
            mvA.lastname = p1.firstname;
            mvA.email = p1.email;

            return mvA;


        }





    }
}
