﻿using Api_machineVirt.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Api_machineVirt.Controllers
{
    public class validController : ApiController
    {

        BDvirtual_mEntities entities = new BDvirtual_mEntities();
        public IEnumerable<valid> Get()
        {
            return entities.valid.ToList();
        }



        [HttpPost]
        public IHttpActionResult Add(Models.Request.Valid_request val_req)
        {
            valid val1 = new valid();
            val1.A_val = val_req.A_val;
            val1.M_val = val_req.M_val;
            val1.id_mach = val_req.id_mach;

            entities.valid.Add(val1);
           
            entities.SaveChanges();
            return Ok("Exito");

        }




    }
}
