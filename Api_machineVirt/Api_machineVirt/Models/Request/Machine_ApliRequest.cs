﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Api_machineVirt.Models.Request
{
    public class Machine_ApliRequest
    {

        public int id_machine { get; set; }
        public Nullable<int> ram { get; set; }
        public Nullable<int> processor { get; set; }
        public string SO { get; set; }
        public string vlan { get; set; }
        public Nullable<int> id_per { get; set; }



        public string password_us { get; set; }
        public string password_adm { get; set; }




        public Nullable<int> tam_disk { get; set; }
        public Nullable<int> id_typeDisk { get; set; }

        public string type_d1 { get; set; }

        public int state_val { get; set; }

        public string name { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string email { get; set; }


        public Nullable<System.DateTime> request_date { get; set; }
        public Nullable<System.DateTime> star_date { get; set; }
        public Nullable<System.DateTime> end_date { get; set; }


    }
}